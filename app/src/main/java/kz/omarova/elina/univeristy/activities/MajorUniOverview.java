package kz.omarova.elina.univeristy.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import kz.omarova.elina.univeristy.R;

public class MajorUniOverview extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_major_uni_overview);

        getSupportActionBar().setTitle("University Major overview");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        TextView major, university, gpa, ielts, sat;
        major = findViewById(R.id.majorTV);
        university = findViewById(R.id.universityTV);
        sat = findViewById(R.id.satTV);
        ielts = findViewById(R.id.ieltsTV);
        gpa = findViewById(R.id.gpaTV);

        gpa.setText(String.valueOf(getIntent().getFloatExtra("APPLY_GPA",4.0f)));
        ielts.setText(String.valueOf(getIntent().getFloatExtra("APPLY_IELTS",9.0f)));
        sat.setText(String.valueOf(getIntent().getIntExtra("APPLY_SAT",1600)));

        major.setText(getIntent().getStringExtra("UNIVERSITY_NAME"));
        university.setText(getIntent().getStringExtra("MAJOR_NAME"));

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }
}
