package kz.omarova.elina.univeristy.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;
import kz.omarova.elina.univeristy.R;
import kz.omarova.elina.univeristy.models.University;

import static kz.omarova.elina.univeristy.globals.UNIVERSITY_NAME;

public class UniversitiesActivity extends AppCompatActivity {

    ListView UniversitiesLV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_universities);
        getSupportActionBar().setTitle("Browse Universities");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        UniversitiesLV = findViewById(R.id.universitiesLV);

        Realm realm = Realm.getDefaultInstance();
        RealmResults<University> universities = realm.where(University.class).findAll();
        ArrayList<String> values = new ArrayList<>();
        for (int i = 0; i < universities.size(); i++) {
            University temp = universities.get(i);
            values.add(temp.getName());
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, values);
        UniversitiesLV.setAdapter(adapter);
        UniversitiesLV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent goToSingleUni = new Intent(getApplicationContext(), SingleUniversityActivity.class);
                goToSingleUni.putExtra(UNIVERSITY_NAME, universities.get(i).getName());
                startActivity(goToSingleUni);
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }
}
