package kz.omarova.elina.univeristy.models;

import io.realm.RealmObject;

public class Major extends RealmObject {

    // name of the major
    private String name;

    // qualification required for this major
    private Qualification qualification;

    // more information can be added here, such as image, description, professor, availability etc.

    public Major() {
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Qualification getQualification() {
        return qualification;
    }

    public void setQualification(Qualification qualification) {
        this.qualification = qualification;
    }
}
