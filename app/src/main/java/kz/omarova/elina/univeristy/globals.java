package kz.omarova.elina.univeristy;

// this file contains string constants used to pass data between intents
// forconvenience and to avoid repetition i've moved them all here
public class globals {
    public static final String APPLY_IELTS = "APPLY_IELTS";
    public static final String APPLY_SAT = "APPLY_SAT";
    public static final String APPLY_GPA = "APPLY_GPA";
    public static final String UNIVERSITY_NAME = "UNIVERSITY_NAME";
    public static final String MAJOR_NAME = "MAJOR_NAME";
}
