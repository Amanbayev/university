package kz.omarova.elina.univeristy.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmList;
import kz.omarova.elina.univeristy.R;
import kz.omarova.elina.univeristy.models.Major;
import kz.omarova.elina.univeristy.models.University;

import static kz.omarova.elina.univeristy.globals.APPLY_GPA;
import static kz.omarova.elina.univeristy.globals.APPLY_IELTS;
import static kz.omarova.elina.univeristy.globals.APPLY_SAT;
import static kz.omarova.elina.univeristy.globals.MAJOR_NAME;
import static kz.omarova.elina.univeristy.globals.UNIVERSITY_NAME;

public class SingleUniversityActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_university);

        // making sure there is back button for Huawei p20Lite and alike phones without soft buttons
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        // get incoming data from previous activity
        Intent incomingIntent = getIntent();
        String name = incomingIntent.getStringExtra(UNIVERSITY_NAME);

        // setup title in action bar
        getSupportActionBar().setTitle("University majors");

        //init realm
        Realm realm = Realm.getDefaultInstance();
        University current = realm.where(University.class).equalTo("name", name).findFirst();
        TextView uniName = findViewById(R.id.universityNameTV);
        uniName.setText(current.getName());

        // querry db for data
        RealmList<Major> majors = current.getMajors();
        ListView majorsLV = findViewById(R.id.majorsSingleUnivLV);
        ArrayList<String> values = new ArrayList<>();
        for (int i = 0; i < majors.size(); i++) {
            values.add(majors.get(i).getName());
        }

        // populate list view with data
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, values);
        majorsLV.setAdapter(adapter);
        majorsLV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent goToOverview = new Intent(getApplicationContext(), MajorUniOverview.class);
                goToOverview.putExtra(UNIVERSITY_NAME, name);
                goToOverview.putExtra(MAJOR_NAME, values.get(position));
                goToOverview.putExtra(APPLY_SAT, majors.get(position).getQualification().getSAT());
                goToOverview.putExtra(APPLY_IELTS, majors.get(position).getQualification().getIELTS());
                goToOverview.putExtra(APPLY_GPA, majors.get(position).getQualification().getGpa());
                // proceed to universal overview activity
                startActivity(goToOverview);
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        // handle back button press
        onBackPressed();
        return super.onSupportNavigateUp();
    }
}
