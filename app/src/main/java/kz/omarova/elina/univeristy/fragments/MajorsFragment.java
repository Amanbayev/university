package kz.omarova.elina.univeristy.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmResults;
import kz.omarova.elina.univeristy.R;
import kz.omarova.elina.univeristy.activities.MajorUniOverview;
import kz.omarova.elina.univeristy.models.Major;
import kz.omarova.elina.univeristy.models.SearchResults;
import kz.omarova.elina.univeristy.models.University;

import static kz.omarova.elina.univeristy.globals.APPLY_GPA;
import static kz.omarova.elina.univeristy.globals.APPLY_IELTS;
import static kz.omarova.elina.univeristy.globals.APPLY_SAT;
import static kz.omarova.elina.univeristy.globals.MAJOR_NAME;
import static kz.omarova.elina.univeristy.globals.UNIVERSITY_NAME;

/**
 * A simple {@link Fragment} subclass.
 */
public class MajorsFragment extends Fragment {
    private ArrayList<String> values = new ArrayList<>();
    private ArrayList<SearchResults> searchResults = new ArrayList<>();
    private int index = 0;

    public MajorsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        float gpa = args.getFloat(APPLY_GPA, 4.0f);
        float ielts = args.getFloat(APPLY_IELTS, 9.0f);
        int sat = args.getInt(APPLY_SAT, 1600);
        Realm realm = Realm.getDefaultInstance();
        RealmResults<Major> majors = realm.where(Major.class)
                .lessThanOrEqualTo("qualification.SAT", sat)
                .lessThanOrEqualTo("qualification.IELTS", ielts)
                .lessThanOrEqualTo("qualification.gpa", gpa)
                .findAll();
        for (int i = 0; i < majors.size(); i++) {
            RealmResults<University> majorUni = realm.where(University.class)
                    .equalTo("majors.name", majors.get(i).getName())
                    .findAll();
            for (int j = 0; j < majorUni.size(); j++) {
                values.add(majors.get(i).getName()+": "+majorUni.get(j).getName());
                searchResults.add(new SearchResults(index, majors.get(i), majorUni.get(j)));
                index++;
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_majors, container, false);
        ListView majorsLV = view.findViewById(R.id.searchResultsMajorsLV);


        ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_1, values);
        majorsLV.setAdapter(adapter);
        majorsLV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent goToOverview = new Intent(getContext(),MajorUniOverview.class);
                goToOverview.putExtra(UNIVERSITY_NAME, searchResults.get(position).getUniversity().getName());
                goToOverview.putExtra(MAJOR_NAME, searchResults.get(position).getMajor().getName());
                goToOverview.putExtra(APPLY_GPA, searchResults.get(position).getMajor().getQualification().getGpa());
                goToOverview.putExtra(APPLY_IELTS, searchResults.get(position).getMajor().getQualification().getIELTS());
                goToOverview.putExtra(APPLY_SAT, searchResults.get(position).getMajor().getQualification().getSAT());
                startActivity(goToOverview);
            }
        });
        return view;
    }

}
