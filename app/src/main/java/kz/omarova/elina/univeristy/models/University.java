package kz.omarova.elina.univeristy.models;

import io.realm.RealmList;
import io.realm.RealmObject;

public class University extends RealmObject {

    // name of the University
    private String name;

    // offered majors with qualifications
    private RealmList<Major> majors;

    // more information can be added here, such as image, description, professor, availability etc.

    public University() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public RealmList<Major> getMajors() {
        return majors;
    }

    public void setMajors(RealmList<Major> majors) {
        this.majors = majors;
    }
}

