package kz.omarova.elina.univeristy.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import kz.omarova.elina.univeristy.R;

import static kz.omarova.elina.univeristy.globals.APPLY_GPA;
import static kz.omarova.elina.univeristy.globals.APPLY_IELTS;
import static kz.omarova.elina.univeristy.globals.APPLY_SAT;

public class ApplyActivity extends AppCompatActivity {
    private float gpa, ielts;
    private int sat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_apply);
        getSupportActionBar().setTitle("Apply and Match");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        Button apply = findViewById(R.id.applyBtn);

        EditText satET, gpaET, ieltsET;
        satET = findViewById(R.id.satET);
        satET.setText("1600");
        gpaET = findViewById(R.id.gpaET);
        gpaET.setText("4.0");
        ieltsET = findViewById(R.id.ieltsET);
        ieltsET.setText("9.0");


        apply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sat = Integer.valueOf(satET.getText().toString());
                gpa = Float.valueOf(gpaET.getText().toString());
                ielts = Float.valueOf(ieltsET.getText().toString());
                // driver intent for search results
                Intent applyResultsIntent = new Intent(getApplicationContext(), ApplyResultsActivity.class);
                applyResultsIntent.putExtra(APPLY_GPA, gpa);
                applyResultsIntent.putExtra(APPLY_IELTS, ielts);
                applyResultsIntent.putExtra(APPLY_SAT, sat);
                startActivity(applyResultsIntent);
            }
        });

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }
}
