package kz.omarova.elina.univeristy.models;

public class SearchResults {
    private int position;
    private Major major;
    private University university;

    public SearchResults(int position, Major major, University university) {
        this.position = position;
        this.major = major;
        this.university = university;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public Major getMajor() {
        return major;
    }

    public void setMajor(Major major) {
        this.major = major;
    }

    public University getUniversity() {
        return university;
    }

    public void setUniversity(University university) {
        this.university = university;
    }
}
