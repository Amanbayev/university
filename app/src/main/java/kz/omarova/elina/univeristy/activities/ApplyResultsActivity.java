package kz.omarova.elina.univeristy.activities;

import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import kz.omarova.elina.univeristy.FragmentPagerAdapter;
import kz.omarova.elina.univeristy.R;

import static kz.omarova.elina.univeristy.globals.APPLY_GPA;
import static kz.omarova.elina.univeristy.globals.APPLY_IELTS;
import static kz.omarova.elina.univeristy.globals.APPLY_SAT;

public class ApplyResultsActivity extends AppCompatActivity {
    private float gpa, ielts;
    private int sat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_apply_results);

        getSupportActionBar().setTitle("Search results");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        // setup tab layout widget from support/design library
        TabLayout tabLayout = findViewById(R.id.tab_layout);
        tabLayout.addTab(tabLayout.newTab().setText("Universities"));
        tabLayout.addTab(tabLayout.newTab().setText("Majors"));

        gpa = getIntent().getFloatExtra(APPLY_GPA,4.0f);
        ielts = getIntent().getFloatExtra(APPLY_IELTS, 9.0f);
        sat = getIntent().getIntExtra(APPLY_SAT,1600);
        Bundle args = new Bundle();
        args.putFloat(APPLY_GPA, gpa);
        args.putFloat(APPLY_IELTS, ielts);
        args.putInt(APPLY_SAT, sat);

        // setup fragments and viewpager
        final ViewPager viewPager = findViewById(R.id.pager);
        final FragmentPagerAdapter adapter = new FragmentPagerAdapter(getSupportFragmentManager(), tabLayout.getTabCount(), args);

        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

        // tab click handler
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }
}
