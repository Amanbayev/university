package kz.omarova.elina.univeristy;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import kz.omarova.elina.univeristy.fragments.MajorsFragment;
import kz.omarova.elina.univeristy.fragments.UniversitiesFragment;

public class FragmentPagerAdapter extends FragmentStatePagerAdapter {
    // basic adapter taken from first popular google search result example
    int numOfTabs;
    Bundle extras;

    public FragmentPagerAdapter(FragmentManager fm, int numOfTabs, Bundle extras){
        super(fm);
        this.extras = extras;
        this.numOfTabs = numOfTabs;
    }

    @Override
    public Fragment getItem(int position) {
        // setup tabs accordingly
        switch (position) {
            case 0:
                UniversitiesFragment tab1 = new UniversitiesFragment();
                tab1.setArguments(extras);
                return tab1;
            case 1:
                MajorsFragment tab2 = new MajorsFragment();
                tab2.setArguments(extras);
                return tab2;
        }
        return null;
    }

    @Override
    public int getCount() {
        return numOfTabs;
    }
}
