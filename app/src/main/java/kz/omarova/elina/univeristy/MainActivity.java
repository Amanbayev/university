package kz.omarova.elina.univeristy;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.yarolegovich.lovelydialog.LovelyInfoDialog;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmList;
import io.realm.RealmResults;
import kz.omarova.elina.univeristy.activities.ApplyActivity;
import kz.omarova.elina.univeristy.activities.UniversitiesActivity;
import kz.omarova.elina.univeristy.models.Major;
import kz.omarova.elina.univeristy.models.Qualification;
import kz.omarova.elina.univeristy.models.University;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // initialize minimum fake data for demo purposes
        initData();

        // set titles in every activity
        getSupportActionBar().setTitle("University Helper");

        // bind buttons for other views
        Button univBtn = findViewById(R.id.button1);
        univBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent goToUniv = new Intent(getApplicationContext(), UniversitiesActivity.class);
                startActivity(goToUniv);
            }
        });

        Button applyBtn = findViewById(R.id.button3);
        applyBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent goToApply = new Intent(getApplicationContext(), ApplyActivity.class);
                startActivity(goToApply);
            }
        });
    }

    private void initData() {

        // setting up Realm ORM
        // https://www.realm.io
        Realm.init(getApplicationContext());
        RealmConfiguration config = new RealmConfiguration.Builder()
                .deleteRealmIfMigrationNeeded()
                .build();
        Realm.setDefaultConfiguration(config);
        Realm realm = Realm.getInstance(config);

        // for testing purposes I'm deleting all data every launch
        realm.beginTransaction();
        realm.deleteAll();
        realm.commitTransaction();

        // check to see if need to initialize data
        boolean needToInit = true;
        final RealmResults<University> universities = realm.where(University.class).findAll();
        if (universities.size() > 0) needToInit = false;

        // if no data - populate
        // in the future here could be a network request for data loading from remote DB
        if (needToInit) {
            // faking data to populate local SQLite Database
            // starting DB transaction
            // using Realm DB ORM library for convenience
            realm.beginTransaction();

            Qualification qual1 = realm.createObject(Qualification.class);
            qual1.setGpa(3.9f);
            qual1.setIELTS(6.5f);
            qual1.setSAT(1450);

            Qualification qual2 = realm.createObject(Qualification.class);
            qual2.setGpa(3.4f);
            qual2.setIELTS(5.5f);
            qual2.setSAT(1250);

            Qualification qual3 = realm.createObject(Qualification.class);
            qual3.setGpa(3.0f);
            qual3.setIELTS(5.0f);
            qual3.setSAT(850);

            Qualification qual4 = realm.createObject(Qualification.class);
            qual4.setGpa(2.5f);
            qual4.setIELTS(4.5f);
            qual4.setSAT(500);

            Qualification elinaQual = realm.createObject(Qualification.class);
            elinaQual.setGpa(3.8f);
            elinaQual.setSAT(1500);
            elinaQual.setIELTS(8.0f);

            Qualification qualRobo = realm.createObject(Qualification.class);
            qualRobo.setGpa(3.8f);
            qualRobo.setIELTS(7.5f);
            qualRobo.setSAT(1400);

            Major cs1 = realm.createObject(Major.class);
            cs1.setName("Computer Science");
            cs1.setQualification(qual1);

            Major cs2 = realm.createObject(Major.class);
            cs2.setName("Information Systems");
            cs2.setQualification(qual2);

            Major phil = realm.createObject(Major.class);
            phil.setName("Philosophy");
            phil.setQualification(qual3);

            Major easy = realm.createObject(Major.class);
            easy.setName("Astronomy");
            easy.setQualification(qual4);

            Major Robots = realm.createObject(Major.class);
            Robots.setName("Robototechnics");
            Robots.setQualification(qualRobo);

            Major elinaMaj = realm.createObject(Major.class);
            elinaMaj.setName("Business");
            elinaMaj.setQualification(elinaQual);

            University nazuni = realm.createObject(University.class);
            nazuni.setName("Nazarbayev University");
            RealmList<Major> majorsNU = new RealmList<>();
            majorsNU.add(Robots);
            majorsNU.add(easy);
            majorsNU.add(elinaMaj);
            majorsNU.add(phil);
            majorsNU.add(cs1);
            nazuni.setMajors(majorsNU);

            University temp1 = realm.createObject(University.class);
            temp1.setName("KazGUU");
            RealmList<Major> majors = new RealmList<>();
            majors.add(cs1);
            majors.add(cs2);
            majors.add(phil);

            temp1.setMajors(majors);

            University enu = realm.createObject(University.class);
            enu.setName("Eurasian National University");
            RealmList<Major> majors2 = new RealmList<>();
            majors2.add(cs1);
            majors2.add(phil);

            enu.setMajors(majors2);

            University kaznu = realm.createObject(University.class);
            kaznu.setName("KazNU by Al-Farabi");
            RealmList<Major> majors3 = new RealmList<>();
            majors3.add(cs1);
            majors3.add(phil);
            majors3.add(easy);
            kaznu.setMajors(majors3);
            // this line is for test purposes only
//          Toast.makeText(getApplicationContext(), "Init data", Toast.LENGTH_SHORT).show();

            // saving transaction to DB
            realm.commitTransaction();
        }
        // else do nothing here
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // setup helper menu item
        getMenuInflater().inflate(R.menu.main_toolbar_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_info_main) {
            Log.i("test", "TEST");
            // display fancy info dialog
            // using LovelyInfoDialog library
            // https://github.com/yarolegovich/LovelyDialog
            new LovelyInfoDialog(this)
                    .setTopColorRes(R.color.colorAccent)
                    .setIcon(R.drawable.logo)
                    .setTitle("University Helper v 1.0")
                    .setMessage("Elina Omarova \nemail: omarova.e@nisa.edu.kz \nThis mobile application helps students discover universities and majors, based on their test scores.")
                    .show();
//            dialog.show();
        }
        return true;
    }
}
