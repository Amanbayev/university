package kz.omarova.elina.univeristy.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmResults;
import kz.omarova.elina.univeristy.R;
import kz.omarova.elina.univeristy.activities.MajorUniOverview;
import kz.omarova.elina.univeristy.models.Major;
import kz.omarova.elina.univeristy.models.SearchResults;
import kz.omarova.elina.univeristy.models.University;

import static kz.omarova.elina.univeristy.globals.APPLY_GPA;
import static kz.omarova.elina.univeristy.globals.APPLY_IELTS;
import static kz.omarova.elina.univeristy.globals.APPLY_SAT;
import static kz.omarova.elina.univeristy.globals.MAJOR_NAME;
import static kz.omarova.elina.univeristy.globals.UNIVERSITY_NAME;

public class UniversitiesFragment extends Fragment {
    private RealmResults<University> universities;
    private ArrayList<String> values = new ArrayList<>();
    private ArrayList<SearchResults> searchResults = new ArrayList<>();
    private int index = 0;

    public UniversitiesFragment() {
        // Required empty public constructor
    }


    public static UniversitiesFragment newInstance(String param1, String param2) {
        UniversitiesFragment fragment = new UniversitiesFragment();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // get arguments from previous screen
        Bundle args = getArguments();
        float gpa = args.getFloat(APPLY_GPA);
        float ielts = args.getFloat(APPLY_IELTS);
        int sat = args.getInt(APPLY_SAT);

        //setup realm ORM
        Realm realm = Realm.getDefaultInstance();
        // querry
        universities = realm.where(University.class)
                .lessThanOrEqualTo("majors.qualification.SAT", sat)
                .lessThanOrEqualTo("majors.qualification.IELTS", ielts)
                .lessThanOrEqualTo("majors.qualification.gpa", gpa)
                .findAll();

        for (int i = 0; i < universities.size(); i++) {
            RealmResults<Major> uniMajor = universities.get(i).getMajors().where()
                    .lessThanOrEqualTo("qualification.SAT", sat)
                    .lessThanOrEqualTo("qualification.IELTS",ielts)
                    .lessThanOrEqualTo("qualification.gpa",gpa)
                    .findAll();
            // populate results
            for (int j = 0; j < uniMajor.size(); j++) {
                values.add(universities.get(i).getName()+": "+uniMajor.get(j).getName());
                searchResults.add(new SearchResults(index, uniMajor.get(j), universities.get(i)));
                index++;
            }
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_universities, container, false);
        ListView universitiesLV = view.findViewById(R.id.searchResultsUniversityLV);

        // populate listview fragment
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_1, values);
        universitiesLV.setAdapter(adapter);
        universitiesLV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // handle click to universal overview activity
                Intent goToOverview = new Intent(getContext(),MajorUniOverview.class);
                goToOverview.putExtra(UNIVERSITY_NAME, searchResults.get(position).getUniversity().getName());
                goToOverview.putExtra(MAJOR_NAME, searchResults.get(position).getMajor().getName());
                goToOverview.putExtra(APPLY_GPA, searchResults.get(position).getMajor().getQualification().getGpa());
                goToOverview.putExtra(APPLY_IELTS, searchResults.get(position).getMajor().getQualification().getIELTS());
                goToOverview.putExtra(APPLY_SAT, searchResults.get(position).getMajor().getQualification().getSAT());
                startActivity(goToOverview);
            }
        });
        return view;
    }

}
