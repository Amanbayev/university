package kz.omarova.elina.univeristy.models;

import io.realm.RealmObject;

public class Qualification extends RealmObject {

    // the scores needed to get major
    private float IELTS;
    private int SAT;
    private float gpa;

    // more information can be added here, like additional options etc.

    public Qualification() {
    }


    public float getIELTS() {
        return IELTS;
    }

    public void setIELTS(float IELTS) {
        this.IELTS = IELTS;
    }

    public int getSAT() {
        return SAT;
    }

    public void setSAT(int SAT) {
        this.SAT = SAT;
    }

    public float getGpa() {
        return gpa;
    }

    public void setGpa(float gpa) {
        this.gpa = gpa;
    }
}
